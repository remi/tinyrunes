import requests, psutil, sys, requests.auth, urllib3, time

print("tinyrunes starting")

port = 0
token = 0
page = 0
userid = 0
champ = 0
lane = "default"

urllib3.disable_warnings()

def get_port_and_token():
    global port,token
    for proc in psutil.process_iter(['name', 'cmdline']):
        if "LeagueClientUx" in proc.info['name']:
            for c in proc.cmdline():
                str = c.split("=")[0][2:]
                if (str == "remoting-auth-token"):
                    token = c.split("=")[1]
                if (str == "app-port"):
                    port = c.split("=")[1]
    if port != 0 and token != 0:
        print(f"found league! port {port}, token {token}")
    else:
        print("could not find league :( is the game running, or are you on anything-but-macos?")
        sys.exit()

def setup_tinyrunes_page():
    global page
    r = local_req("get", "lol-perks/v1/pages")
    chk = False
    for rune in r:
        if rune["name"] == "tinyrunes":
            chk = True
            page = rune["id"]
    if (chk == False):
        print("trying to make a tinyrunes page...")
        success = make_tinyrunes(8200, [8214,
                            8009,
                            8017,
                            8210,
                            8226,
                            8237,
            5005,
            5008,
            5002], 8000)
        if (success):
            print("made tinyrunes page!")
        else:
            print("tinyrunes rune page not found, and we couldent make one. please make a rune page called \"tinyrunes\"! (are you below level 10?)")
            sys.exit()
    else:
        print("rune page found!")

def setup_user_id():
    global userid
    r = local_req("get", "lol-summoner/v1/current-summoner")
    if ("summonerId" in r and "displayName" in r):
        userid = r["summonerId"]
        print(f"welcome, {r['displayName']}!")
    else:
        print("couldent find summonerId... are you logged in?")
        sys.exit()

def make_tinyrunes(primary, selected, sub):
    global page
    rune = {
        "name": "tinyrunes",
        "primaryStyleId": primary,
        "selectedPerkIds": selected,
        "subStyleId": sub
    }
    r = local_req("post", "lol-perks/v1/pages", rune)
    if ("id" in r and r["isValid"] == True):
        page = r["id"]
        return True
    else:
        return False

def delete_tinyrunes():
    local_req("delete", f"lol-perks/v1/pages/{page}")
    pass

def wait_for_chara():
    global champ
    done = False
    print("ready to grab you some runes~")
    while (not done):
        champselect = local_req("get", "lol-champ-select/v1/session")
        if ("myTeam" in champselect):
            for member in champselect["myTeam"]:
                if (member["summonerId"] == userid and member["championId"] != 0):
                    if (member["championId"] != champ):
                        champ = member["championId"]
                        gettem_runes()
        time.sleep(1)

def gettem_runes():
    print(f"setting runes for champ {champ}")
    roons = requests.get(f"https://axe.lolalytics.com/mega/?ep=rune&p=d&v=1&cid={champ}&lane={lane}").json()
    if not "summary" in roons and lane != "default":
        print(f"{lane} runes not found, trying default")
        roons = requests.get(f"https://axe.lolalytics.com/mega/?ep=rune&p=d&v=1&cid={champ}&lane=default").json()
    if not "summary" in roons:
        print("runes not found, giving up :(")
        sys.exit()
    roon = roons["summary"]["runes"]["pick"]["set"]
    pag = roons["summary"]["runes"]["pick"]["page"]
    pri = [8000, 8100, 8200, 8400, 8300][pag["pri"]]
    sub = [8000, 8100, 8200, 8400, 8300][pag["sec"]]
    sel = [*roon["pri"], *roon["sec"], *roon["mod"]]
    print(f"setting the runes...")
    delete_tinyrunes()
    print(pri,sel,sub)
    make_tinyrunes(pri, sel, sub)
    print("ready!")

def local_req(type, url, data = ""):
    if type == "post":
        r = requests.post(f"https://127.0.0.1:{port}/{url}", auth=requests.auth.HTTPBasicAuth("riot", token), verify=False, json=data)
    elif type == "get":
        r = requests.get(f"https://127.0.0.1:{port}/{url}", auth=requests.auth.HTTPBasicAuth("riot", token), verify=False)
    elif type == "delete":
        r = requests.delete(f"https://127.0.0.1:{port}/{url}", auth=requests.auth.HTTPBasicAuth("riot", token), verify=False)
    ret = "bruh"
    try:
        ret = r.json()
    except:
        ret = r.text()
    finally:
        return ret

get_port_and_token()
setup_user_id()
setup_tinyrunes_page()
wait_for_chara()